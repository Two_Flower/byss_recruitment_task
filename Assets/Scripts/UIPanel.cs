﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIPanel : MonoBehaviour
{
    public virtual void Show(bool withChildren)
    {
        Image myImage = GetComponent<Image>();
        Color imageColor = myImage.color;
        imageColor.a = 1;
        myImage.color = imageColor;

        if (withChildren)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(true);
            }
        }
        gameObject.SetActive(true);
        GetComponent<Animator>().SetTrigger("Show");
    }

    public void Hide()
    {
        GetComponent<Animator>().SetTrigger("Hide");
    }

    public void Evt_MyDestroy()
    {
        StartCoroutine(HidePanel());
    }

    protected virtual IEnumerator HidePanel()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        Image myImage = GetComponent<Image>();
        Color imageColor = myImage.color;
        imageColor.a = 0;
        myImage.color = imageColor;

        yield return new WaitForSeconds(0.1f);
        gameObject.SetActive(false);
    }
}
