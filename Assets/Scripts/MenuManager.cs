﻿using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance = null;

    public int OptionsNumber { private set; get; }
    private OptionButton _currentButton;

    [SerializeField]
    private GameObject _optionWindow;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    public void UpdateOptionsAmount(int optionsNumber)
    {
        OptionsNumber = optionsNumber;
    }

    public void UpdateOptionsAmount(string amountTxt)
    {
        if (amountTxt.Length > 0 && amountTxt[0] != '-')
        {
            int optionsNumber = int.Parse(amountTxt);
            UpdateOptionsAmount(optionsNumber);
        }
        else
        {
            OptionsNumber = 0;
        }
    }

    public OptionButton GetCurrentButton()
    {
        return _currentButton;
    }

    public void OpenOptionWindow(OptionButton buttonClicked)
    {
        _currentButton = buttonClicked;
        _optionWindow.GetComponent<OptionWindow>().Show(true);
    }

    public void SaveOptionText(string textToSave)
    {
        _currentButton.SaveOptionText(textToSave);
    }
}
