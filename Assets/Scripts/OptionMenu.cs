﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionMenu : UIPanel
{
    [SerializeField]
    private GameObject _optionButtonPrefab;
    [SerializeField]
    private GameObject _closeButton;
    [SerializeField]
    private GameObject _optionsContent;
    [SerializeField]
    private GameObject _scrollHandle;

    private List<GameObject> optionButtons;

    public void Evt_ShowOptions()
    {
        optionButtons = new List<GameObject>();
        _closeButton.transform.SetParent(transform, false);
        for (int i = 0; i < MenuManager.instance.OptionsNumber; i++)
        {
            GameObject optionObj = Instantiate(_optionButtonPrefab);
            optionObj.transform.SetParent(_optionsContent.transform, false);
            optionButtons.Add(optionObj);
            Text buttonTxt = optionObj.GetComponentInChildren<Text>();
            buttonTxt.text = "Przycisk " + (i + 1).ToString();
            OptionButton optionButton = optionObj.GetComponent<OptionButton>();
            optionButton.SetButton(i);
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        _closeButton.transform.SetParent(_optionsContent.transform, false);
    }

    public override void Show(bool withChildren)
    {
        _scrollHandle.SetActive(true);
        base.Show(withChildren);
    }

    protected override IEnumerator HidePanel()
    {
        for (int i = 0; i < optionButtons.Count; i++)
        {
            Destroy(optionButtons[i]);
        }
        optionButtons.Clear();
        _scrollHandle.SetActive(false);

        return base.HidePanel();
    }
}
