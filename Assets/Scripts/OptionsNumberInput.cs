﻿using UnityEngine;
using UnityEngine.UI;

public class OptionsNumberInput : UIPanel
{
    public void ControlOptionsNumberDisplay(string input)
    {
        if (input.Length > 0)
        {
            if (input[0] == '-')
                GetComponentInChildren<InputField>().text = "0";
        }
    }
}
