﻿using UnityEngine;

public class OptionButton : MonoBehaviour
{
    private int _myNumber;

    public void SetButton(int number)
    {
        _myNumber = number;
    }

    public int GetNumber()
    {
        return _myNumber;
    }

    public string GetOptionText()
    {
        return PlayerPrefs.GetString("Button" + _myNumber.ToString(), "Enter text...");
    }

    public void OpenOptionWindow()
    {
        MenuManager.instance.OpenOptionWindow(this);
    }

    public void SaveOptionText(string textToSave)
    {
        PlayerPrefs.SetString("Button" + _myNumber.ToString(), textToSave);
    }
}
