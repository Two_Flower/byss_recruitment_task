﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UIAnimation : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private Animator _myAnim;

    private void Start()
    {
        _myAnim = GetComponent<Animator>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_myAnim)
            _myAnim.SetBool("Pressed", true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (_myAnim)
            _myAnim.SetBool("Pressed", false);
    }
}
