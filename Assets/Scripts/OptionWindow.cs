﻿using UnityEngine;
using UnityEngine.UI;

public class OptionWindow : UIPanel
{
    [SerializeField]
    private Text _title;

    public override void Show(bool withChildren)
    {
        base.Show(true);
        OptionButton currentButton = MenuManager.instance.GetCurrentButton();
        _title.text = "Przycisk " + (currentButton.GetNumber() + 1).ToString();
        GetComponentInChildren<InputField>().text = currentButton.GetOptionText();
    }
}
